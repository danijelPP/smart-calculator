from PyQt5.QtWidgets import QApplication, QMainWindow, QAction, QLabel
from PyQt5.QtGui import QIcon, QImage, QPainter, QPen
from PyQt5.QtCore import *
import sys
import numpy as np
import Trainer
from src.components import ANN
import DataCollector


def prepare_data_for_classifying(data_in, M):
    data = data_in.copy()
    Tc = np.mean(data, axis=0)
    # print(Tc)
    position_correction = np.tile(Tc, (np.shape(data)[0], 1))
    data -= position_correction
    maxAbsValue = abs(max(data.max(), data.min(), key=abs))
    scale_correction = np.identity(2) * (1.0 / maxAbsValue)
    data = np.matmul(data, scale_correction)
    D = 0.0
    distances_list = list()
    distances_list.append(0.0)
    for i in range(np.shape(data)[0] - 1):
        D += np.linalg.norm(data[i + 1] - data[i])
        distances_list.append(D)
    final_points = list()
    for k in range(0, M):
        distance = (k * 1.0 / (M - 1)) * D
        point = DataCollector.get_point(data, distance, distances_list)
        final_points.append(point[0])
        final_points.append(point[1])
    return final_points


class Classifier:
    def __init__(self, trained_ann_path="./saves/trained_ann.txt"):
        self.architecture = None
        self.trained_ann_path = trained_ann_path
        self.weights_matrices = []
        self.parse_config_data()
        self.ann = ANN.ANN()
        self.ann.import_pre_trained(self.architecture, self.weights_matrices)

    def classify(self, current_line_data, M):
        final_points = prepare_data_for_classifying(current_line_data, M)
        # print(final_points)
        Xd = np.array(final_points)
        # print(Xd)
        layers_outs = self.ann.forward_pass(Xd)
        out = layers_outs[-1][0]
        percentage = max(out) / sum(out)
        return np.argmax(out) + 1, percentage

    def regression(self, input_):
        Xd = input_
        # print(Xd)
        layers_outs = self.ann.forward_pass(Xd)
        out = layers_outs[-1]
        return out[0][0]

    def parse_config_data(self):
        config_file = open(self.trained_ann_path)
        lines = config_file.readlines()
        parts = [int(Trainer.keep_only_number(x)) for x in lines[0].split(",")]
        self.architecture = parts
        for layer in range(1, len(self.architecture)):
            line = lines[layer].rstrip()
            # print(line)
            spliced = line[:-1].split(",")
            parts = [float(Trainer.keep_only_number(x)) for x in spliced]
            W = np.array(parts)
            W = W.reshape(self.architecture[layer - 1], self.architecture[layer])
            # print(W)
            self.weights_matrices.append(W)


# noinspection PyUnresolvedReferences
class AnnPainterClassifier(QMainWindow):
    def __init__(self, classifier_mode=False, number_of_classes=5, M=50, names_of_classes=None,
                 save_data_path="./saves/saved_data.txt", trained_ann_path="./saves/trained_ann.txt"):
        super().__init__()
        top = 200
        left = 600
        width = 800
        height = 600

        icon = "./images/link.jpg"
        self.number_of_classes = number_of_classes
        self.names_of_classes = names_of_classes
        self.M = M
        self.classifier = Classifier(trained_ann_path=trained_ann_path)
        self.save_data_path = save_data_path

        self.setWindowTitle("ANN Painter - Classifier")
        self.setGeometry(left, top, width, height)
        # fixed size
        self.setFixedSize(width, height)
        self.setWindowIcon(QIcon(icon))
        # paint field aka image
        self.image = QImage(self.size(), QImage.Format_ARGB32)
        self.image.fill(Qt.white)

        self.drawing = False
        self.brushSize = 2
        self.brushColor = Qt.black
        self.lastPoint = QPoint()

        # data
        self.current_class = None
        self.current_class_percentage = 0
        self.current_line_data = None
        self.thread_pool = QThreadPool()
        print("Multithreading with maximum %d threads" % self.thread_pool.maxThreadCount())

        # menu bar
        self.mainMenu = self.menuBar()
        fileMenu = self.mainMenu.addMenu("File")

        self.current_class_label = QLabel(
            "Detected class: " + str(self.current_class) + "\tAssurance percentage:" +
            str(self.current_class_percentage * 100))

        clearAction = QAction(text="Clear", parent=self)
        clearAction.setShortcut("Ctrl+C")
        fileMenu.addAction(clearAction)
        clearAction.triggered.connect(self.clear)

        self.toolBar = self.addToolBar("Current Class")
        self.toolBar.addWidget(self.current_class_label)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.clear()
            self.drawing = True
            self.lastPoint = event.pos()
            painter = QPainter(self.image)
            painter.setPen(QPen(self.brushColor, self.brushSize, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
            painter.drawPoint(self.lastPoint)
            self.current_line_data = np.array([[self.lastPoint.x() * 1.0, self.lastPoint.y()]])
            # print(self.lastPoint)
            # print(self.current_line_data)
            # print(self.current_class)
            self.update()

    def mouseMoveEvent(self, event):
        if (event.buttons() & Qt.LeftButton) & self.drawing:
            painter = QPainter(self.image)
            painter.setPen(QPen(self.brushColor, self.brushSize, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
            painter.drawLine(self.lastPoint, event.pos())
            self.lastPoint = event.pos()
            self.current_line_data = np.append(self.current_line_data, [[self.lastPoint.x(), self.lastPoint.y()]],
                                               axis=0)
            # print(self.lastPoint)
            self.update()

    def mouseReleaseEvent(self, event):
        detected_class, assurance = self.classifier.classify(self.current_line_data, self.M)
        self.set_class(detected_class, assurance)
        if event.button == Qt.LeftButton:
            self.drawing = False

    def paintEvent(self, event):
        canvasPainter = QPainter(self)
        canvasPainter.drawImage(self.rect(), self.image, self.image.rect())

    def clear(self):
        self.image.fill(Qt.white)
        self.current_line_data = None
        self.update()

    def set_class(self, c, assurance):
        self.current_class = c
        self.current_class_percentage = assurance * 100
        name = str(self.current_class)
        if self.names_of_classes is not None:
            name = self.names_of_classes[c - 1]
        self.current_class_label = QLabel(
            "Detected class: " + name + "\tAssurance percentage:" +
            str(self.current_class_percentage) + "%")
        self.toolBar.clear()
        self.toolBar.addWidget(self.current_class_label)

    def build_lambda(self, c):
        return lambda: self.set_class(c)

    def show_error(self, error_description):
        pass


if __name__ == "__main__":
    app = QApplication(sys.argv)
    # window = AnnPainterClassifier(names_of_classes=["α(alpha)", "β(beta)", "γ(gamma)", "δ(delta)", "ε(epsilon)"])
    window = AnnPainterClassifier(
        names_of_classes=["0_down_left", "0_down_right", "0_up_left", "0_up_right", "1", "2", "3", "4", "5", "6", "7",
                          "8_up_left", "8_up_right", "8_down_left", "8_down_right", "9", "_(from_left)",
                          "_(from_right)",
                          "|(from_up)", "|(from_down)", "\\(from_down)"],
        trained_ann_path="saves/trained_ann.txt")
    window.show()
    app.exec()
