from Classifier import *
from src.components.Sign import *
from src.components.Expression import *
import math


# noinspection PyUnresolvedReferences
class AnnCalculator(QMainWindow):
    def __init__(self, classifier_mode=False, number_of_classes=5, M=50, names_of_classes=None,
                 save_data_path="./saves/saved_data.txt", trained_ann_path="./saves/trained_ann.txt"):
        super().__init__()
        top = 200
        left = 400
        width = 800
        height = 300

        icon = "./images/link.jpg"
        self.number_of_classes = number_of_classes
        self.names_of_classes = names_of_classes
        self.M = M
        self.classifier = Classifier(trained_ann_path=trained_ann_path)
        self.save_data_path = save_data_path
        self.expressionHandler = ExpressionHandler()

        self.setWindowTitle("Hand Written Calculator")
        self.setGeometry(left, top, width, height)
        # fixed size
        self.setFixedSize(width, height)
        self.setWindowIcon(QIcon(icon))

        # paint field aka image
        self.image = QImage(QSize(width, height), QImage.Format_ARGB32)
        self.image.fill(Qt.white)
        self.painter = QPainter()

        self.drawing = False
        self.brushSize = 4
        self.brushColor = Qt.black
        self.lastPoint = QPoint()

        # data
        self.current_class = None
        self.current_class_percentage = 0
        self.current_line_data = None
        self.line_data_stack = []
        self.thread_pool = QThreadPool()
        print("Multithreading with maximum %d threads" % self.thread_pool.maxThreadCount())

        # menu bar
        self.mainMenu = self.menuBar()
        self.mainMenu.setNativeMenuBar(False)
        fileMenu = self.mainMenu.addMenu("File")

        self.current_class_label = QLabel(
            "Detected class: " + str(self.current_class) + "\tAssurance percentage:" +
            str(self.current_class_percentage * 100))

        clearAction = QAction(text="Clear", parent=self)
        clearAction.setShortcut("Ctrl+C")
        self.mainMenu.addAction(clearAction)
        clearAction.triggered.connect(self.clear)

        self.undoAction = QAction(text="Delete last input", parent=self)
        self.undoAction.setShortcut("Ctrl+Z")
        # TODO
        # self.mainMenu.addAction(self.undoAction)
        self.undoAction.triggered.connect(self.undo)
        self.undoAction.setEnabled(False)
        self.toolBar = self.addToolBar("Current Class")
        self.toolBar.addWidget(self.current_class_label)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            # clear panel
            # self.clear()
            self.drawing = True
            self.lastPoint = event.pos()
            painter = QPainter(self.image)
            painter.setRenderHint(QPainter.Antialiasing)
            painter.setPen(QPen(self.brushColor, self.brushSize, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
            painter.drawPoint(self.lastPoint)
            self.current_line_data = np.array([[self.lastPoint.x() * 1.0, self.lastPoint.y() * 1.0]])
            self.undoAction.setEnabled(True)
            # print(self.lastPoint)
            # print(self.current_line_data)
            # print(self.current_class)
            self.update()

    def mouseMoveEvent(self, event):
        if (event.buttons() & Qt.LeftButton) & self.drawing:
            painter = QPainter(self.image)
            painter.setRenderHint(QPainter.Antialiasing)
            painter.setPen(QPen(self.brushColor, self.brushSize, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
            painter.drawLine(self.lastPoint, event.pos())
            self.lastPoint = event.pos()
            self.current_line_data = np.append(self.current_line_data, [[self.lastPoint.x(), self.lastPoint.y()]],
                                               axis=0)
            # print(self.lastPoint)
            self.update()

    def mouseReleaseEvent(self, event):
        l = len(self.current_line_data)
        if self.current_line_data is not None:
            self.line_data_stack.append(self.current_line_data.copy())
        if len(self.current_line_data) == 1:
            self.set_class(-1, 1)
            self.expressionHandler.handle_input(0, self.current_line_data[0], 1)
        else:
            location = self.current_line_data[l // 2].copy()
            distance_vector = [self.current_line_data[-1][0] - self.current_line_data[0][0],
                               self.current_line_data[-1][1] - self.current_line_data[0][1]]
            size = math.sqrt(distance_vector[0] ** 2 + distance_vector[1] ** 2)
            detected_class, assurance = self.classifier.classify(self.current_line_data, self.M)
            self.set_class(detected_class, assurance)
            self.expressionHandler.handle_input(detected_class, location, size)
            self.current_line_data = None
        if event.button == Qt.LeftButton:
            self.drawing = False

    def undo(self):
        if len(self.line_data_stack) == 0:
            return
        line_to_delete = self.line_data_stack.pop()
        painter = QPainter(self.image)
        painter.setPen(QPen(Qt.white, self.brushSize, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
        first_point_ = QPointF(line_to_delete[0][0], line_to_delete[0][1])
        painter.drawPoint(first_point_)
        if len(line_to_delete) > 1:
            for point in line_to_delete[:]:
                second_point_ = QPointF(point[0], point[1])
                painter.drawLine(first_point_, second_point_)
                first_point_ = second_point_
        if len(self.line_data_stack) == 0:
            self.undoAction.setEnabled(False)
        self.update()

    def paintEvent(self, event):
        canvasPainter = QPainter(self)
        canvasPainter.drawImage(self.rect(), self.image, self.image.rect())

    def clear(self):
        self.expressionHandler.clear()
        self.image.fill(Qt.white)
        self.current_line_data = None
        self.undoAction.setEnabled(False)
        self.update()

    def set_class(self, c, assurance):
        self.current_class = c
        self.current_class_percentage = assurance * 100
        if c == -1:
            self.current_class_label = QLabel(
                "Detected class: " + "None class(one dot)" + "\tAssurance percentage:" +
                str(self.current_class_percentage) + "%")
        else:
            name = str(self.current_class)
            if self.names_of_classes is not None:
                name = self.names_of_classes[c - 1]
            self.current_class_label = QLabel(
                "Detected class: " + name + "\tAssurance percentage:" +
                str(self.current_class_percentage) + "%")
        self.toolBar.clear()
        self.toolBar.addWidget(self.current_class_label)

    def build_lambda(self, c):
        return lambda: self.set_class(c)

    def show_error(self, error_description):
        pass


if __name__ == "__main__":
    app = QApplication(sys.argv)
    # window = AnnPainterClassifier(names_of_classes=["α(alpha)", "β(beta)", "γ(gamma)", "δ(delta)", "ε(epsilon)"])
    window = AnnCalculator(
        names_of_classes=[str(s) for s in BasicSign],
        trained_ann_path="saves/trained_ann.txt")
    window.show()
    app.exec()
