import numpy as np


def get_class(one_hot_notation):
    numbers = one_hot_notation[1:-1]
    numbers = numbers.replace('.', '')
    numbers = numbers.replace(' ', '')
    class_ = numbers.index('1') + 1
    return class_


def new_class_oh_notation(new_class_number, one_hot_notation):
    yd = np.zeros((1, new_class_number), float)
    class_ = get_class(one_hot_notation)
    yd[0][class_ - 1] = 1.0
    return yd[0]


def reformat(file_path, new_class_number):
    """
    If there are new classes introduced, old saved data must be reformated
    :param file_path:
    :param new_class_number:
    :return:
    """
    myfileRead = open(file_path, "r+")
    lines = myfileRead.readlines()
    lines = [line.split("\t") for line in lines]
    previous_classes_notation = [line[0] for line in lines]
    shape_data = [line[1] for line in lines]
    new_classes_notation = [new_class_oh_notation(new_class_number, prev) for prev in previous_classes_notation]
    # print(new_classes_notation)
    myfileRead.close()
    myfileWrite = open(file_path, "w")
    for ind in range(len(shape_data)):
        n = new_classes_notation[ind]
        n_c_n = str(n)
        n_c_n = n_c_n.replace("\n", "")
        if n_c_n.count("\n") > 0:
            print("Error, line break because of array length, I tried :(")
            raise InterruptedError("Not good data format")
        myfileWrite.write(n_c_n + "\t" + shape_data[ind])
    myfileWrite.close()
    # fix your own mess
    remove_break(file_path)


def remove_data_for_class(file_path, class_):
    """
    If some class is badly learned, delete previous data set for her
    :param file_path:
    :param new_class_number:
    :return:
    """
    myfileRead = open(file_path, "r+")
    lines = myfileRead.readlines()
    lines = [line.split("\t") for line in lines]
    previous_classes_notation = [line[0] for line in lines]
    shape_data = [line[1] for line in lines]
    lines_to_remove = []
    for i in range(len(shape_data)):
        if get_class(previous_classes_notation[i]) == class_:
            lines_to_remove.append(i)
    for r in sorted(lines_to_remove, reverse=True):
        shape_data.pop(r)
        previous_classes_notation.pop(r)
    # print(new_classes_notation)
    myfileRead.close()
    myfileWrite = open(file_path, "w")
    for ind in range(len(shape_data)):
        myfileWrite.write(previous_classes_notation[ind] + "\t" + shape_data[ind])
    myfileWrite.close()


def remove_break(file_path):
    """
    If some class is badly learned, delete previous data set for her
    :param file_path:
    :param new_class_number:
    :return:
    """
    myfileRead = open(file_path, "r+")
    lines = myfileRead.readlines()
    # print(new_classes_notation)
    myfileRead.close()
    myfileWrite = open(file_path, "w")
    for ind in range(len(lines) - 1):
        cur = lines[ind]
        nex = lines[ind + 1]
        if lines[ind].startswith(" "):
            print("em", ind)
            continue
        if lines[ind + 1].startswith(" "):
            print(ind)
            myfileWrite.write(lines[ind][:-1] + lines[ind + 1])
        else:
            myfileWrite.write(lines[ind])
    myfileWrite.close()


if __name__ == '__main__':
    # reformat("saves/saved_data_with_directions.txt",28)
    # remove_data_for_class("saves/saved_data_with_directions.txt", class_=8)
    remove_break("saves/saved_data_with_directions.txt")
