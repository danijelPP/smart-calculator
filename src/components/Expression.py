import math
import sys
from enum import Enum
import shlex
from src.components.Sign import *
import operator


class ExpressionHandler:
    def __init__(self):
        self.expression: [Sign] = []
        self.sorted_expression: [Sign] = []
        self.currentResult = 0

    def clear(self):
        self.expression = []
        self.sorted_expression = []
        self.currentResult = 0

    def handle_input(self, input_class: int, location: list, size):
        if input_class == 0:
            # decimal dot
            self.expression.append(Sign(".", location, size))
        elif input_class <= BasicSign.digit_0_up_right.value:
            self.expression.append(Sign("0", location, size))
        elif input_class <= BasicSign.digit_7.value:
            self.expression.append(Sign(str(input_class - 4), location, size))
        elif input_class <= BasicSign.digit_8_down_right.value:
            self.expression.append(Sign(str(8), location, size))
        elif input_class <= BasicSign.digit_9.value:
            self.expression.append(Sign(str(9), location, size))
        elif input_class <= BasicSign.horizontal_bar_from_right.value:
            self.check_for_complex_sign("-", size, location)
        elif input_class <= BasicSign.vertical_bar_from_down.value:
            self.check_for_complex_sign("|", size, location)
        elif input_class <= BasicSign.backslash_from_up.value:
            self.check_for_complex_sign("\\", size, location)
        elif input_class <= BasicSign.slash_from_up.value:
            self.check_for_complex_sign("/", size, location)
        elif input_class <= BasicSign.left_bracket_from_up.value:
            self.expression.append(Sign("(", location, size))
        elif input_class <= BasicSign.right_bracket_from_up.value:
            self.expression.append(Sign(")", location, size))
        self.calculate_if_possible()

    def check_for_complex_sign(self, input_sign, size, location: list):
        """
        Check if + or * is drawn
        :param input_sign:
        :param size:
        :param location: list
        :return:
        """
        for i in range(len(self.expression)):
            sign_b = self.expression[i].sign_str
            location_b = self.expression[i].location
            size_b = self.expression[i].size
            distance_vector = ([location[0] - location_b[0], location[1] - location_b[1]])
            distance = math.sqrt(distance_vector[0] ** 2 + distance_vector[1] ** 2)
            if distance < 100:
                if sign_b in ["-"] and input_sign in ["|", "\\", "//"] or sign_b in ["|", "\\",
                                                                                     "//"] and input_sign == "-":
                    self.expression[i].sign_str = "+"
                    return
                elif sign_b == "/" and input_sign == "\\" or sign_b == "\\" and input_sign == "/":
                    self.expression[i].sign_str = "*"
                    return
        self.expression.append(Sign(input_sign, location, size))

    def calculate_if_possible(self):
        self.sorted_expression = sorted(self.expression)
        expressionString: str = ""
        for e in self.sorted_expression:
            expressionString += e.sign_str
        try:
            self.currentResult = eval(expressionString)
            sys.stdout.write('\r' + expressionString + "=" + str(self.currentResult))
            # print(expressionString + "=" + str(self.currentResult))
        except Exception:
            sys.stdout.write('\r' + "Invalid expression:" + expressionString)
            # print("Invalid expression:" + expressionString)
