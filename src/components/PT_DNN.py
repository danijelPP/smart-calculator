import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt


class PTDeep(nn.Module):
    def __init__(self, dimensions, activation_fun):
        """Arguments:
           - D: dimensions of each datapoint
           - C: number of classes
        """
        super().__init__()
        if len(dimensions) < 2:
            raise ValueError("There should be at least two layers, input and output")
        self.weights, self.biases = create_parameter_lists(dimensions)
        self.activation_fun = activation_fun

    def forward(self, X):
        in_data = X
        # prođi kroz skrivene slojeve
        for layer in range(len(self.weights) - 1):
            in_data = self.activation_fun(torch.mm(in_data, self.weights[layer]) + self.biases[layer])
        # izlazni sloj
        s = torch.mm(in_data, self.weights[-1]) + self.biases[-1]
        return torch.softmax(s, dim=1)
        # torch.nn.CrossEntropyLoss
        # unaprijedni prolaz modela: izračunati vjerojatnosti
        #   koristiti: torch.mm, torch.softmax

    def get_loss(self, X, Yoh_):
        in_data = X
        # prođi kroz skrivene slojeve
        for layer in range(len(self.weights) - 1):
            w = self.weights[layer]
            b = self.biases[layer]
            in_data = self.activation_fun(torch.mm(in_data, w) + b)
        # izlazni sloj
        s = torch.mm(in_data, self.weights[-1]) + self.biases[-1]
        e = (torch.sum(torch.mul(-s, Yoh_), 1) + torch.logsumexp(s, 1))
        loss = torch.mean(e, dim=0)
        return loss


def get_all_parameters_in_order(weights, biases):
    # order should be weight0,bias0,weight1,....
    params = list()
    for i in range(len(weights)):
        params.append(weights[i])
        params.append(biases[i])
    return params


def create_parameter_lists(dim):
    weights = nn.ParameterList([nn.Parameter(torch.rand(dim[i - 1], dim[i])) for i in range(1, len(dim))])
    biases = nn.ParameterList([nn.Parameter(torch.rand(dim[i])) for i in range(1, len(dim))])
    return weights, biases


def train(model, X, Yoh_, param_niter, param_delta, param_lambda=1e-4, show=False):
    """Arguments:
       - X: model inputs [NxD], type: torch.Tensor
       - Yoh_: ground truth [NxC], type: torch.Tensor
       - param_niter: number of training iterations
       - param_delta: learning rate
    """

    # inicijalizacija optimizatora
    parameters = get_all_parameters_in_order(model.weights, model.biases)
    optimizer = optim.SGD(parameters, lr=param_delta, weight_decay=param_lambda)
    learning_rate_lowered = False
    # petlja učenja
    for i in range(param_niter):
        # ispisujte gubitak tijekom učenja
        # afin regresijski model

        loss = model.get_loss(X, Yoh_)

        if loss < 0.1 and not learning_rate_lowered:
            for g in optimizer.param_groups:
                g['lr'] *= 0.5
            learning_rate_lowered = True

        probs = eval(model, X)
        # graficki prikaz
        if show and (i < 10 or (i % 10 == 0 and i < 100) or (i < 1000 and i % 100 == 0) or i % 1000 == 0):
            Y = probs.argmax(axis=1)
            decfun = pt_logreg_decfun(model, 1)
            bbox = (np.min(X.detach().numpy(), axis=0), np.max(X.detach().numpy(), axis=0))
            Y_ = np.argmax(Yoh_.detach().numpy(), axis=1)
            plt.pause(0.001)
            plt.clf()

        # računanje gradijenata
        loss.backward()

        print(f'step: {i}, loss:{loss}')
        # korak optimizacije
        optimizer.step()

        # Postavljanje gradijenata na nulu
        optimizer.zero_grad()


def eval(model, X):
    """Arguments:
       - model: type: PTLogreg
       - X: actual datapoints [NxD], type: np.array
       Returns: predicted class probabilites [NxC], type: np.array
    """
    X_tensor = torch.tensor(X).float()
    return model.forward(X_tensor).detach().numpy()
    # ulaz je potrebno pretvoriti u torch.Tensor
    # izlaze je potrebno pretvoriti u numpy.array
    # koristite torch.Tensor.detach() i torch.Tensor.numpy()


def pt_logreg_decfun(model, class_index):
    def classify(X):
        return eval(model, X)

    # [:, class_index]

    return classify
