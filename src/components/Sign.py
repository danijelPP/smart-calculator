from enum import Enum


class BasicSign(Enum):
    digit_0_down_left = 1
    digit_0_down_right = 2
    digit_0_up_left = 3
    digit_0_up_right = 4
    digit_1 = 5
    digit_2 = 6
    digit_3 = 7
    digit_4 = 8
    digit_5 = 9
    digit_6 = 10
    digit_7 = 11
    digit_8_up_left = 12
    digit_8_up_right = 13
    digit_8_down_left = 14
    digit_8_down_right = 15
    digit_9 = 16
    horizontal_bar_from_left = 17
    horizontal_bar_from_right = 18
    vertical_bar_from_up = 19
    vertical_bar_from_down = 20
    backslash_from_down = 21
    backslash_from_up = 22
    slash_from_down = 23
    slash_from_up = 24
    left_bracket_from_down = 25
    left_bracket_from_up = 26
    right_bracket_from_down = 27
    right_bracket_from_up = 28


class Sign:
    def __init__(self, sign_str, location, size):
        self.sign_str = sign_str
        self.size = size
        self.location = location

    def __lt__(self, other):
        return self.location[0] < other.location[0]
