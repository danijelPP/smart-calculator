import numpy as np

np.set_printoptions(linewidth=np.inf)
np.set_printoptions(threshold=np.inf)


def sigmoid(x):
    return 1.0 / (1 + np.exp(-x))


def sigmoid_derived(x):
    return x * (1 - x)


class ANN:

    def __init__(self, dataset=None, M=None, architecture=None, algorithm=2, step=1, batch_size=1,
                 smart_data_selection=False):
        if dataset is not None:
            self.inputs = dataset[0]
            self.true_outputs = dataset[1]
            self.architecture = architecture
            self.M = M
            self.algorithm = algorithm
            self.batch_size = batch_size
            self.smart_data_selection = smart_data_selection
            self.p = self.inputs.shape[0]
            self.n = self.inputs.shape[1]
            self.m = architecture[-1]
            if (self.M != -1 and self.M * 2 != architecture[0]) or architecture[0] != self.n:
                raise Exception("ANN architecture isn't compatible with dataset")
            if self.algorithm == 3 and self.smart_data_selection:
                self.inputs, self.true_outputs = self.smart_selection(self.inputs, self.true_outputs)
            self.weights_matrices = self.create_weight_matrices()
        self.algorithm = algorithm
        self.step = step

    def import_pre_trained(self, architecture, weights_matrices):
        self.architecture = architecture
        self.weights_matrices = weights_matrices
        self.n = architecture[0]
        self.m = architecture[-1]

    def forward_pass(self, input_layer):
        input_into_layer = input_layer
        layers_outputs = []
        for layer in range(len(self.architecture) - 1):
            z1 = input_into_layer.dot(self.weights_matrices[layer])
            output = sigmoid(z1)
            if len(output.shape) == 1:
                output = output.reshape(1, output.shape[0])
            layers_outputs.append(output)
            input_into_layer = output
        # print(layers_outputs)
        return layers_outputs

    def train(self, max_iterations):
        self.weights_matrices = self.create_weight_matrices()
        for i in range(max_iterations):
            ann_error = 0.0
            for k in range(0, self.p, self.batch_size):
                x = np.array(self.inputs[k:k + self.batch_size])
                y = np.array(self.true_outputs[k:k + self.batch_size])
                layers_outputs = self.forward_pass(x)
                deltas = []
                next_level_delta = None
                for layer in range(len(self.architecture) - 2, -1, -1):
                    if layer == len(self.architecture) - 2:
                        error = y - layers_outputs[layer]
                        delta = error * sigmoid_derived(layers_outputs[layer])
                        if len(delta.shape) == 1:
                            delta = delta.reshape(1, delta.shape[0])
                        deltas.append(delta)
                        next_level_delta = delta
                    else:
                        error = next_level_delta.dot(self.weights_matrices[layer + 1].T)
                        delta = error * sigmoid_derived(layers_outputs[layer])
                        if len(delta.shape) == 1:
                            delta = delta.reshape(1, delta.shape[0])
                        next_level_delta = delta
                        deltas.append(delta)
                layers_adjustments = []
                input_layer = x
                if len(input_layer.shape) == 1:
                    input_layer = input_layer.reshape(1, input_layer.shape[0])
                deltas.reverse()
                for layer in range(0, len(self.architecture) - 1):
                    if layer == 0:
                        input_into_layer = input_layer
                    else:
                        input_into_layer = layers_outputs[layer - 1]
                    layers_adjustments.append(input_into_layer.T.dot(deltas[layer]))
                ann_error += np.linalg.norm(layers_outputs[-1] - y) ** 2
                for layer in range(0, len(self.architecture) - 1):
                    self.weights_matrices[layer] += self.step * layers_adjustments[layer]
            ann_error = ann_error / (2 * self.p)
            print(str(i)+" :: Error:", ann_error)

    def smart_selection(self, inputs, true_outputs):
        Y = np.empty((0, self.m), float)
        X = np.empty((0, inputs[0].shape[0]), float)
        number_of_batches = self.p // self.batch_size
        number_per_class = self.p // self.m
        number_per_class_in_batch = self.batch_size // self.m
        for b in range(number_of_batches):
            for c in range(self.m):
                for i in range(number_per_class_in_batch):
                    X = np.vstack([X, inputs[c * number_per_class + b * number_per_class_in_batch + i]])
                    Y = np.vstack([Y, true_outputs[c * number_per_class + b * number_per_class_in_batch + i]])
        return X, Y

    def save_ann_config(self, config_save_path):
        config_file = open(config_save_path, "w")
        config_file.write(str(self.architecture) + "\n")
        for W in self.weights_matrices:
            flatten = W.flatten()
            for num in flatten:
                config_file.write(str(num) + ",")
            config_file.write("\n")
            # config_file.write(str(W.flatten()) + "\n")

    def create_weight_matrices(self):
        w_m = []
        for layer in range(1, len(self.architecture)):
            w_m.append(
                np.random.rand(self.architecture[layer - 1], self.architecture[layer]) / self.architecture[layer - 1])
        return w_m

    def create_weight_matrices_zeros(self):
        w_m = []
        for layer in range(1, len(self.architecture)):
            w_m.append(
                np.zeros((self.architecture[layer - 1], self.architecture[layer])))
        return w_m
