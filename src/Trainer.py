from src.components.ANN import *
import numpy as np
import sys


def keep_only_number(xd):
    only_number = ""
    for c in xd:
        if c in ['[', ']', '(', ')', ' ', '\n']:
            continue
        only_number += c
    return only_number


class Trainer:
    def __init__(self, dataset_path="./saves/data.txt", M=50, architecture=None, algorithm=2, step=0.7,
                 trained_ann_path="./saves/trained_ann.txt"):
        self.dataset_path = dataset_path
        self.config_save_path = trained_ann_path
        self.M = M
        self.architecture = architecture
        self.m = architecture[-1]
        self.algorithm = algorithm
        self.step = step
        self.data = self.parse()

    def parse(self):
        data_file = open(self.dataset_path, "r")
        lines = data_file.readlines()
        Y = np.empty((0, self.m), float)
        X = None
        for line in lines:
            parts = line.split("\t")
            y = parts[0]
            splits = y.split(" ")
            yd = np.array([float(keep_only_number(x)) for x in splits])
            # print(yd)
            Y = np.vstack([Y, yd])
            # print(yd)
            Xd = (parts[1].rstrip()).split(",")
            # print(Xd)
            Xd = np.array([float(keep_only_number(xd)) for xd in Xd])
            if X is None:
                X = np.empty((0, Xd.shape[0]), float)
            X = np.vstack([X, Xd])
            # print(Xd)
        # print(Y)
        # print(X)
        return X, Y

    def train(self, max_iterations=1000, max_time=None, max_error=1e-3):
        batch_size = 1
        smart_data_selection = False
        if self.algorithm == 1:
            batch_size = self.data[0].shape[0]
            print(batch_size)
            # print(batch_size)
        if self.algorithm == 3:
            batch_size = 10
            smart_data_selection = True
        ann = ANN(self.data, self.M, self.architecture, self.algorithm, self.step, batch_size, smart_data_selection)
        ann.train(max_iterations)
        ann.save_ann_config(self.config_save_path)
        return ann


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Please provide arguments: data_path M architecture(nxlx..xm) algorithm_number")
        exit(-1)
    data_path = sys.argv[1]
    M = int(sys.argv[2])
    architecture = [int(x) for x in sys.argv[3].split("x")]
    algorithm = int(sys.argv[4])
    # "./saves/data.txt"
    trainer = Trainer(data_path, M, architecture, algorithm)
    trainer.train(2500)
