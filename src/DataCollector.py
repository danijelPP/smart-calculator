from PyQt5.QtWidgets import QApplication, QMainWindow, QMenu, QMenuBar, QAction, QFileDialog, QLabel, QWidgetAction, \
    QToolBar
from PyQt5.QtGui import QIcon, QImage, QPainter, QPen
from PyQt5.QtCore import *
import sys
import numpy as np
import Reformator
from src.components.Sign import *

def prepare_data(data, M):
    Tc = np.mean(data, axis=0)
    # print(Tc)
    position_correction = np.tile(Tc, (np.shape(data)[0], 1))
    data -= position_correction
    maxAbsValue = abs(max(data.max(), data.min(), key=abs))
    scale_correction = np.identity(2) * (1.0 / maxAbsValue)
    data = np.matmul(data, scale_correction)
    D = 0.0
    distances_list = list()
    distances_list.append(0.0)
    for i in range(np.shape(data)[0] - 1):
        D += np.linalg.norm(data[i + 1] - data[i])
        distances_list.append(D)
    final_points = list()
    for k in range(0, M):
        distance = (k * 1.0 / (M - 1)) * D
        point = get_point(data, distance, distances_list)
        final_points.append((point[0], point[1]))
    return final_points


def get_point(data, distance, distances_list):
    mid = 0
    start = 0
    end = len(distances_list)
    step = 0

    while start <= end:
        step = step + 1
        mid = (start + end) // 2
        if abs(distance - distances_list[mid]) < 0.00001:
            return data[mid]
        if mid < len(distances_list) - 1:
            if distances_list[mid + 1] > distance > distances_list[mid]:
                if abs(distance - distances_list[mid]) < abs(distances_list[mid + 1] - distance):
                    return data[mid]
                else:
                    return data[mid + 1]
        if distance < distances_list[mid]:
            end = mid - 1
        else:
            start = mid + 1
    return None


class DataSaver(QRunnable):
    def __init__(self, current_line_data, current_class, M, number_of_classes, save_data_path="./saves/saved_data.txt"):
        super().__init__()
        self.data = current_line_data
        self.current_class = current_class
        self.M = M
        self.number_of_classes = number_of_classes
        self.save_data_path = save_data_path

    @pyqtSlot()
    def run(self):
        final_points = prepare_data(self.data, self.M)
        yd = np.zeros((1, self.number_of_classes), float)
        yd[0][self.current_class - 1] = 1.0
        with open(self.save_data_path, "a") as myfile:
            myfile.write("\n" + str(yd[0]) + "\t" + str(final_points))


# noinspection PyUnresolvedReferences
class AnnPainterCollector(QMainWindow):
    def __init__(self, classifier_mode=False, number_of_classes=10, M=50, names_of_classes=None,
                 save_data_path="./saves/saved_data.txt"):
        super().__init__()
        top = 200
        left = 600
        width = 800
        height = 600

        icon = "./images/link.jpg"
        self.number_of_classes = number_of_classes
        self.names_of_classes = names_of_classes
        self.M = M
        self.save_data_path = save_data_path

        Reformator.reformat(save_data_path, number_of_classes)

        self.setWindowTitle("ANN Painter - Data Collector")
        self.setGeometry(left, top, width, height)
        # fixed size
        self.setFixedSize(width, height)
        self.setWindowIcon(QIcon(icon))
        # paint field aka image
        self.image = QImage(self.size(), QImage.Format_ARGB32)
        self.image.fill(Qt.white)

        self.drawing = False
        self.brushSize = 2
        self.brushColor = Qt.black
        self.lastPoint = QPoint()

        # data
        self.current_class = 1
        self.current_class_label = None
        self.current_line_data = None
        self.thread_pool = QThreadPool()
        print("Multithreading with maximum %d threads" % self.thread_pool.maxThreadCount())

        # menu bar
        self.mainMenu = self.menuBar()
        fileMenu = self.mainMenu.addMenu("File")
        self.classMenu = self.mainMenu.addMenu("Class")
        self.add_classes_actions()

        saveAction = QAction(text="Select Save Data Path", parent=self)
        saveAction.setShortcut("Ctrl+S")
        fileMenu.addAction(saveAction)
        saveAction.triggered.connect(self.save)

        saveDataAction = QAction(text="Save Data", parent=self)
        saveDataAction.setShortcut("Ctrl+Shift+S")
        fileMenu.addAction(saveDataAction)
        saveDataAction.triggered.connect(self.save_data)

        clearAction = QAction(text="Clear", parent=self)
        clearAction.setShortcut("Ctrl+C")
        fileMenu.addAction(clearAction)
        clearAction.triggered.connect(self.clear)

        self.toolBar = self.addToolBar("Current Class")
        self.set_class(1)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.clear()
            self.drawing = True
            self.lastPoint = event.pos()
            painter = QPainter(self.image)
            painter.setPen(QPen(self.brushColor, self.brushSize, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
            painter.drawPoint(self.lastPoint)
            self.current_line_data = np.array([[self.lastPoint.x() * 1.0, self.lastPoint.y()]])
            # print(self.lastPoint)
            # print(self.current_line_data)
            # print(self.current_class)
            self.update()

    def mouseMoveEvent(self, event):
        if (event.buttons() & Qt.LeftButton) & self.drawing:
            painter = QPainter(self.image)
            painter.setPen(QPen(self.brushColor, self.brushSize, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
            painter.drawLine(self.lastPoint, event.pos())
            self.lastPoint = event.pos()
            self.current_line_data = np.append(self.current_line_data, [[self.lastPoint.x(), self.lastPoint.y()]],
                                               axis=0)
            # print(self.lastPoint)
            self.update()

    def mouseReleaseEvent(self, event):
        if event.button == Qt.LeftButton:
            self.drawing = False

    def paintEvent(self, event):
        canvasPainter = QPainter(self)
        canvasPainter.drawImage(self.rect(), self.image, self.image.rect())

    def save_image(self):
        filePath, _ = QFileDialog.getSaveFileName(self, "Save image", "",
                                                  "PNG(*.png);;JPEG(*.jpg *.jpeg;;ALL Files(*.*) ")
        if filePath == "":
            return
        self.image.save(filePath)

    def save(self):
        filePath, _ = QFileDialog.getOpenFileName(self, "Select Save Data Path", "",
                                                  "TXT(*.txt);;ALL Files(*.*) ")
        if filePath == "":
            return
        self.save_data_path = filePath

    def save_data(self):
        data_saver = DataSaver(self.current_line_data, self.current_class, self.M, self.number_of_classes,
                               self.save_data_path)
        self.thread_pool.start(data_saver)

    def clear(self):
        self.image.fill(Qt.white)
        self.current_line_data = None
        self.update()

    def set_class(self, c):
        self.current_class = c
        name = str(self.current_class)
        if self.names_of_classes is not None:
            name = str(c) + " - " + self.names_of_classes[c - 1]
        self.current_class_label = QLabel("Selected class: " + name)
        self.toolBar.clear()
        self.toolBar.addWidget(self.current_class_label)

    def add_classes_actions(self):
        for c in range(1, self.number_of_classes + 1):
            # class_action = None
            if self.names_of_classes is not None:
                class_action = QAction(text=str(c) + " - " + self.names_of_classes[c - 1], parent=self)
            else:
                class_action = QAction(text=str(c), parent=self)
            class_action.triggered.connect(self.build_lambda(c))
            self.classMenu.addAction(class_action)

    def build_lambda(self, c):
        return lambda: self.set_class(c)

    def show_error(self, error_description):
        pass


if __name__ == "__main__":
    app = QApplication(sys.argv)
    # window = AnnPainterCollector(names_of_classes=["α(alpha)", "β(beta)", "γ(gamma)", "δ(delta)", "ε(epsilon)"])
    window = AnnPainterCollector(
        names_of_classes=[str(s) for s in BasicSign],
        number_of_classes=28, save_data_path="saves/saved_data_with_directions.txt")
    window.show()
    app.exec()
