import Trainer
import sys
import numpy as np
import Classifier


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print("Please provide arguments: data_path M architecture(nxlx..xm) algorithm_number")
        exit(-1)
    trained_ann_path = "./saves/simple_trained_ann.txt"
    # train ann
    data_path = sys.argv[1]
    M = int(sys.argv[2])
    architecture = [int(x) for x in sys.argv[3].split("x")]
    algorithm = int(sys.argv[4])
    trainer = Trainer.Trainer(data_path, M, architecture, algorithm, trained_ann_path=trained_ann_path,
                              step=1)
    ann = trainer.train(100000)

    # test ann
    classifier = Classifier.Classifier(trained_ann_path)
    print(classifier.ann.architecture)
    test_inputs = [1, 0.7, 0.1, 0.11, -0.1, -0.7]
    results = [classifier.regression(np.array([x])) for x in test_inputs]
    print(test_inputs)
    print(results)
